// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
/*
exports.creandoUsuario = functions.firestore.document('usuarios/{userId}').onCreate((snapshot, context) => {
    
    // Agregamos un atributo nuevo que indique la fecha de inicio de sesion
    // y un flag para indicar que ha iniciado una sesion
    return snapshot.ref.set({
        inicio: snapshot.data().fecha,
        loggedin: true
    }, {merge: true});
});
// Listen for changes in all documents in the 'users' collection
exports.actualizandoUsuario = functions.firestore
.document('usuarios/{userId}')
.onUpdate((change, context) => {
    // If we set `/users/marie` to {name: "Marie"} then
    // context.params.userId == "marie"
    // ... and ...
    // change.after.data() == {name: "Marie"}
    const beforeFecha = change.before.data().fecha;
    const afterFecha = change.after.data().fecha;
    console.log('afterFecha', afterFecha);
    console.log('beforeFecha', beforeFecha);
    const elapsedMilliseconds = new Date(afterFecha).getTime() - new Date(beforeFecha).getTime();
    const elapsedTime = (elapsedMilliseconds / 1000) / 60; // obtenemos los minutos transcurridos
    console.log('elapsedTime', elapsedTime);
    // comprobamos si hay que iniciar una sesion o no
    if (elapsedTime > 40) {
        // si el tiempo transcurrido es superior a los 40 min, entonces se trata de una nueva sesion
        return afterSnap.ref.set({
            inicio: afterFecha,
            loggedin: true
        }, {merge: true});
    }else if(elapsedTime > 0 && elapsedTime < 30){
        // si el tiempo transcurrido es menor a 30 min, entonces se trata de una sesion ya iniciada
        if(change.after.data().inicio === undefined){
            //si el documento no tiene el campo inicio, lo agregamos
            return change.after.ref.set({
                inicio: afterFecha,
                loggedin: true
            }, {merge: true});
        }
    }else if(elapsedTime >= 30){
        // si el tiempo transcurrido es superior o igual a 30, entonces hay que cerrar la sesion
        if(change.after.data().inicio !== undefined){
            // si si tiene el campo inicio, calculamos el tiempo transcurrido para
            // saber si ha terminado su sesion
            const sesionMillis = new Date(afterFecha).getTime() - 
            new Date(change.after.data().inicio).getTime();
            const tiempoSession = (sesionMillis / 1000) / 60; // tiempo de sesion en minutos
            if(tiempoSesion >= 30) {
                // si la sesion es igual o superior a los 30 min, marcamos el fin de la misma
                return change.after.ref.set({
                    fin: afterFecha,
                    loggedin: false
                }, {merge: true});
            }
        }
    }
    return Promise.resolve(change.after);
});*/
