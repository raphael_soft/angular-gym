// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCmYKVenNB_LQ5aSm2z6w7cZcI_FilVPVs",
    authDomain: "play-b0c7a.firebaseapp.com",
    databaseURL: "https://play-b0c7a.firebaseio.com",
    projectId: "play-b0c7a",
    storageBucket: "play-b0c7a.appspot.com",
    messagingSenderId: "821651470893",
    appId: "1:821651470893:web:c2e194a359d63b2a1996a4"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
