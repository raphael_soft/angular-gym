import { Usuario } from './usuario.model';
import { Observable } from 'rxjs';
export class Banda {
    id: number;
    mac: string; // campo indispensable. Es calculado. Contiene la frecuencia maxima para un usuario
    numero: string; // campo indispensable. Es calculado. Contiene las intensidades, u objetivos propios de un usuario
    usuario: Observable<Usuario>;
    nombre: string;

    constructor() {
        this.id = 0;
        this.mac = '';
        this.numero = '';
    }
}
