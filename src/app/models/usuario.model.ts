import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { map, retry, catchError } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, throwError } from 'rxjs';
import {MatSnackBar} from '@angular/material/snack-bar';

export class Usuario {
    id: string;
    maxFrecuency: number; // campo indispensable. Es calculado. Contiene la frecuencia maxima para un usuario
    intensities: any[]; // campo indispensable. Es calculado. Contiene las intensidades, u objetivos propios de un usuario
    _objetivo: any;
    tiempo: number; // campo indispensable. Es calculado. Aloja el tiempo de la sesión del usuario
    baseColor: number; // campo indispensable. Es calculado. Contiene el color de inicio para este usuario
    intensity: any; // campo indispensable. Es calculado. Contiene el objetivo en el cual se encuentra el usuario
    // de firebase
    banda: number;
    edad: number;
    email: string;
    fecha: any;
    inicio: any;
    nombre: string;
    sexo: string;
    nick: string;
    objetivo: string;
    ritmo: number;
    timer: any; // id de setInterval()
    colors = ['amarillo', 'azul', 'rojo', 'violeta', 'verde']; // array de colores utilizados
    db: AngularFirestore;
    http: HttpClient;
    snack: MatSnackBar;

    constructor() {
        console.log('creando instancia Usuario de ' + this.id);
        this.id = null;
        this.maxFrecuency = 0;
        this.intensities = [];
        this._objetivo = {
            value: 0,
            text: '',
            objetivo: ''
        };
        this.tiempo = 0;
        this.baseColor = 0;
        this.intensity = null;
        this.banda = 0;
        this.edad = 0;
        this.email = '';
        this.fecha = null;
        this.inicio = null;
        this.nombre = '';
        this.nick = '';
        this.objetivo = '';
        this.ritmo = 0;
        this.sexo = 'M';
        this.timer = setInterval(() => {
            this.calculateTime();
            // comprobamos que el tiempo de sesion del usuario no sea igual o mayor a 30 min
            if (this.tiempo >= 30) {
              // Si el tiempo de sesion es igual o mayor a 30, generamos el informe para este usuario
              console.log('El usuario ' + this.nombre + ' tiene 30 o más minutos de sesión.');
              // buscamos si en el array de informes se encuentra el id del usuario, si no esta, generamos el informe
              if (!this.tieneInforme()) {
                console.log('El usuario ' + this.nombre + ' no ha generado su informe aun. Se generará su informe');
                this.generateInforme();
              } else {
                console.log('El usuario ' + this.nombre + ' ya tiene un informe realizado en el dia de hoy.');
              }

              // console.log('index de usuario ' + usuario.nombre + ' en las sesiones es: ' + index);
              if (this.timer !== null) {
                console.log('eliminando intervalo de chequeo de tiempo del usuario ', this.nombre);
                clearInterval(this.timer);
                this.timer = null;
                /* y sacamos del array de sesiones la sesion del usuario */
                // console.log('removiendo sesion de usuario');
              }
            }
        }, 10000);

        this.calculateTime();
        // calculamos la frecuencia maxima del usuario
        this.getMaxPulseFrecuency();
        // calculamos el color base del usuario segun su edad, se usa el ultimo digito de la misma
        this.generateBaseColor();
        // obtenemos las intensidades u objetivos para este usuario
        this.initIntensities();
        // obtenemos en cual objetivo o intensidad se encuentra, segun su ritmo cardiaco actual
        this.calculateIntensity();
        // obtenemos el objeto objetivo del usuario segun el objetivo (numero) establecido para él
        this.calculateObjetivo();
        // console.log('usuario', usuario);
    }

    /*
    Funcion para calcular el tiempo de la sesion del usuario en minutos
    */
    public calculateTime() {
        if (this.inicio && this.fecha) {
          const inicio = this.inicio.toMillis();
          // const fin = this.fecha.toMillis();
          const fin = new Date().getTime();
          const timeMillis = fin - inicio;
          this.tiempo = (timeMillis / 1000) / 60;
          console.log('calculando tiempo de ' + this.nombre + '. Su tiempo es: ' + this.tiempo + ' minutos');
        }
    }

    /*
    Funcion para saber si el usuario ya ha generado un informe en el dia de hoy
    */
    public tieneInforme(): boolean {
        const storageObject: any = localStorage.getItem(this.id);
        if (storageObject) {
        if (storageObject === this.fecha.toLocaleString()) {
            console.log(`El usuario ${this.nombre} ya tiene un informe generado en el día de hoy`);
            return true;
        } else {
            console.log(`El usuario ${this.nombre} no tiene un informe generado en el día de hoy
            pero si de la fecha ${storageObject}`);
            localStorage.removeItem(this.id);
        }
        } else {
        console.log(`El usuario ${this.nombre} no tiene informes generados anteriormente`);
        }
        return false;
    }

    /*
    Funcion para generar informe de la sesion del usuario
    */
    public generateInforme(reintento: number = 1) {
        const fechaInicio = this.inicio.toMillis();
        const sesionTime = 30 * 60 * 1000;
        // fecha con 30 min adicionales a fechaInicio
        const fechaFin = new Date(fechaInicio + sesionTime);
        const pulsaciones = [];
        const estadisticas = [];
        let endDate: any;
        let beginDate: any;
        let lastPulse = '';
        let gap = 0;
        // guardamos en local bd el id de usuario junto a la fecha para marcar que ya se ha generado su informe
        localStorage.setItem(this.id, this.fecha.toLocaleString());
        // Obtenemos las pulsaciones del usuario desde la fecha de inicio de la sesion hasta 30 minutos despues
        /*this.db.doc('usuarios/' + this.id).collection('pulsaciones',
        ref => ref.where('fecha', '>=', this.inicio).where('fecha', '<=', fechaFin)).
        get().subscribe(results => {
        if (!results) {
            console.log('No hay pulsaciones para enviar informe al usuario ' + this.nombre);
            this.snack.open('No hay pulsaciones para enviar informe al usuario ' + this.nombre, 'OK', {
            duration: 2000,
            });
            return;
        }
        results.forEach(doc => {
            pulsaciones.push({id: doc.id, ...doc.data()});
        });
        if (!pulsaciones[0]) {
            console.log('No hay pulsaciones para enviar informe al usuario ' + this.nombre);
            this.snack.open('No hay pulsaciones para enviar informe al usuario ' + this.nombre, 'OK', {
            duration: 2000,
            });
            return;
        }
        /*
        El siguiente proceso calcula el tiempo que se dedicó a cada objetivo
        
        beginDate = pulsaciones[0].fecha;
        lastPulse = pulsaciones[0].ritmo;
        let duracion = 0; // guarda la duracion de tiempo en un objetivo
        // iteramos todos los objetivos o intensidades del usuario
        this.intensities.forEach((intensity, index) => {
            duracion = 0;
            // iteramos todas las pulsaciones
            for (let i = 1; i < pulsaciones.length; i++) {
            endDate = pulsaciones[i].fecha;
            // gap es el periodo de tiempo entre la ultima pulsacion y la actual
            gap = (endDate.toMillis() - beginDate.toMillis());
            /*
            si el ritmo de la pulsacion actual es >= al ultimo ritmo y <= al ritmo del objetivo actual
            sumamos el gap de tiempo a la duracion
            
            if (pulsaciones[i].ritmo >= lastPulse && pulsaciones[i].ritmo <= intensity.value) {
                duracion += gap;
            }
            beginDate = endDate;
            }
            // al finalizar la iteracion de las pulsaciones, insertamos en un array las estadisticas obtenidas
            estadisticas.push({
            tiempo: Math.floor((duracion / 1000) / 60), // tiempo de duracion en este objetivo, en minutos
            objetivo: intensity.text, // nombre del objetivo
            ritmo: intensity.value // ritmo cardiaco del objetivo
            });
            lastPulse = intensity.value;
        });
        // Enviar estadisticas para generar grafica
        const data = {
            data: estadisticas,
            user: this,
            colors: this.colors
        };
        this.enviarCorreo(data);
        });
        */
    }
    private enviarCorreo(datos: any, reintentos: number = 1) {
        const httpOptions = {
        headers: new HttpHeaders({
            'Content-Type':  'application/json',
        })
        };
        // console.log('datos a enviar', datos);
        if (reintentos > 3) {
            return null;
        }
        /*
        this.http.post<any>('http://experienciaplay.com/gym-api/api/sendReporte', datos, httpOptions)
        .pipe(retry(3), catchError(this.handleError)).subscribe(result => {
            if (result !== undefined) {
                // console.log('result', result);
                if (result.resp === 'OK') {
                console.log('Informe enviado satisfactoriamente');
                this.snack.open('Informe enviado satisfactoriamente', 'OK', {
                    duration: 2000,
                });
                return;
                } else {
                /*
                si no se logra enviar el informe, se muestra un mensaje y se reintenta hasta 3 veces.
                
                console.log('Informe no enviado debido a un error', result.message);
                this.snack.open('Informe no enviado debido a un error en los datos', 'OK', {
                    duration: 2000,
                });
                this.enviarCorreo(datos, reintentos++);
                }
            }
        }, error => {
            /*
            si no se logra enviar el informe, se muestra un mensaje y se reintenta hasta 3 veces.
            
            console.error('Error al enviar informe por correo');
            this.snack.open('Error al enviar informe por correo', 'OK', {
                duration: 2000,
            });
            this.enviarCorreo(datos, reintentos++);
        });*/
    }

    /*
    Funcion para obtener la imagen a mostrar como indicador (flecha arriba, flecha abajo o check)
    */
    public getIndicador(): any {
        const indicador = {
        icon: 'assets/images/arriba.png'
        }; // icono a mostrar, flecha arriba o flecha abajo
        const intensity = this.intensity;
        const objetivo = Number(this.objetivo);
        const ritmo = Number(this.ritmo);
        if (intensity !== undefined && intensity !== null) {
        if (ritmo < this._objetivo.value) {
            indicador.icon = 'assets/images/arriba.png';
        } else if (ritmo >= this._objetivo.value &&
            ((objetivo < this.intensities.length) &&
            (ritmo < this.intensities[objetivo].value)) ||
            (this.intensities.length === objetivo)) {
                indicador.icon = 'assets/images/check.png';
        } else {
            indicador.icon = 'assets/images/abajo.png';
        }
        }
        return indicador;
    }
    /*
    Funcion para obtener el objetivo en el cual se encuentra el usuario, segun su ritmo cardiaco
    */
    public calculateIntensity(): any {
        const ritmo = Number(this.ritmo);
        let last = null;
        if (this.intensities !== undefined && this.intensities != null) {
            this.intensities.forEach(intensity => {
                if (ritmo >= intensity.value || last == null) {
                    last = intensity;
                }
            });
        }
        this.intensity = new Object(last);
    }
    /*
    Funcion para obtener los datos del objetivo establecido para el usuario
    */
    public calculateObjetivo() {
        const objetivo = Number(this.objetivo);
        if (this.intensities !== undefined && this.intensities != null && this.intensities[objetivo - 1] !== undefined) {
            this._objetivo = this.intensities[objetivo - 1];
            this._objetivo.value = Math.floor(this._objetivo.value);
        }
    }
    /*
    Funcion para obtener la frecuencia maxima del usuario
    */
    public getMaxPulseFrecuency() {
        let frecuency = 0;
        if (this.sexo === 'M') {
        frecuency = 213 - (0.789 * Number(this.edad));
        }
        if (this.sexo === 'F') {
        frecuency = 208 - (0.789 * Number(this.edad));
        }
        this.maxFrecuency = Math.floor(frecuency);
    }
    /*
    Funcion para crear el array de objetivos o intensidades para un usuario
    */
    public initIntensities() {
        const values = [];
        const gaps = [59, 69, 79, 89, 100];
        let intensity = null;
        gaps.forEach(value => {
        const calc = Math.floor((value * this.maxFrecuency) / 100);
        intensity = JSON.parse(JSON.stringify({
            value: 0,
            text: '',
            objetivo: '',
            color: ''
        }));

        if (value >= 0 && value <= 59) {
            intensity.color = this.baseColor;
            intensity.text = 'Reduciendo Stress';
            intensity.objetivo = 'Reducir Stress';
        }
        if (value >= 60 && value <= 69) {
            intensity.color = this.getNextColor(this.baseColor, 1);
            intensity.text = 'Moviéndose Más';
            intensity.objetivo = 'Mover Más';
        }
        if (value >= 70 && value <= 79) {
            intensity.color = this.getNextColor(this.baseColor, 2);
            intensity.text = 'Perdiendo Peso';
            intensity.objetivo = 'Perder Peso';
        }
        if (value >= 80 && value <= 89) {
            intensity.color = this.getNextColor(this.baseColor, 3);
            intensity.text = 'Tonificando';
            intensity.objetivo = 'Tonificar';
        }
        if (value >= 90 && value <= 100) {
            intensity.color = this.getNextColor(this.baseColor, 4);
            intensity.text = 'Mejorando Rendimiento';
            intensity.objetivo = 'Mejorar Rendimiento';
        }
        intensity.value = calc;
        values.push(intensity);
        });

        this.intensities = values;
    }
    /*
    Funcion para generar el color base de un usuario
    Se obtiene un numero entre 0 y 4
    */
    public generateBaseColor() {
        const segundoDigito = this.edad % 10;
        this.baseColor = Math.floor(segundoDigito / 2);
    }
    /*
    Funcion para obtener el color hexadecimal segun una posicion
    */
    public getColor() {
        return this.colors[this.intensity.color];
    }
    /*
    Funcion para obtener el siguiente color en el array de colores, segun la posicion de otro color
    */
    public getNextColor(colorPos, positions) {
        let currPos = colorPos;
        for (let i = 0; i < positions; i++) {
        if (currPos === this.colors.length - 1) {
            currPos = 0;
        } else {
            currPos++;
        }
        }
        return currPos;
    }

    private handleError(error: HttpErrorResponse) {
        let mensaje = 'Error al hacer la peticion al servidor';
        if (error.error instanceof ErrorEvent) {
        // A client-side or network error occurred. Handle it accordingly.
        mensaje = error.error.message;
        console.error('An error occurred:', error.error.message);
        } else {
        // The backend returned an unsuccessful response code.
        // The response body may contain clues as to what went wrong,
        console.error(
            `Backend returned code ${error.status}, ` +
            `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError('Something bad happened; please try again later.');
    }
}
