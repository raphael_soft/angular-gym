import { Usuario } from './usuario.model';
import { Observable } from 'rxjs';
export class Session {
    user: Usuario;
    timer: any;
    interval: any;

    constructor() {
        this.user = null;
        this.timer = null;
        this.interval = null;
    }
}
