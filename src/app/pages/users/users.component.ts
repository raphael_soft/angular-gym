import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, throwError, from } from 'rxjs';
import * as firebase from 'firebase/app';
import { map, retry, catchError } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';
import 'firesql/rx'; // <-- Important! Don't forget
import 'firebase/firestore';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import { Session } from '../../models/session.model';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})

export class UsersComponent implements OnInit {
  bandas: Observable<any[]>; // array de bandas existentes en base de datos Mysql
  bandasPlain: any[];
  /*
  IMPORTANTE: para que este sistema funcione bien, es importante que el cliente tenga como zona horaria
  Buenos Aires ya que esta es la zona horaria usada en Firestore.
  */
  numFilas: number; // contiene el numero total de bandas, usado en la grilla del front
  numColumnas = 4; // contiene el numero de columnas a mostrar en la grilla
  fecha: Date; // La fecha de hoy con la hora en 00:00:00
  sessions: Array<any>; // array que guarda las sesiones de intervalos para cada usuario
  informes: any[]; // array de id's de usuarios a quienes ya se les han enviado informes
  colors = ['amarillo', 'azul', 'rojo', 'violeta', 'verde']; // array de colores utilizados
  constructor(private db: AngularFirestore, private http: HttpClient, private snack: MatSnackBar,
              private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.informes = [];
    this.sessions = [];
    this.bandasPlain = [];
    const today = new Date();
    let usuario = null;
    today.setHours(0, 0, 0); // establecemos la hora a 00:00:00
    this.fecha = today; // formato d-m-Y 00:00:00
    // Seleccionamos las bandas existentes en la base de datos mysql, usando la api
    this.bandas = this.http.get<any[]>('https://experienciaplay.com/gym-api/api/get/bandas')
    // iteramos cada banda obtenida con el operador map()
    .pipe(retry(3), map(bandasN => bandasN.map(banda => {
      // por cada banda, inicializamos un objeto llamado aux con los datos de la banda y de su usuario actual
      const aux: any = {
        id: banda.id,
        mac: banda.mac,
        numero: banda.nombre,
        usuario: {
          intensity: {
            value: 0,
            text: '',
            objetivo: '',
            color: '',
            tiempo: 0
          }
        }
      };
      /*
        Seleccionamos el usuarios en firestore que tengan el numero de banda igual a la banda actual
      */
      this.db.collection('usuarios',
      ref => ref.where('banda', '==', String(aux.numero))
      // Cuidamos que la fecha del usuario sea del dia de hoy
      .where('fecha', '>=', this.fecha)
      // y ordenamos los resultados segun la fecha (porque pueden haber varios) y seleccionamos solo el ultimo
      .orderBy('fecha', 'desc').limit(1))
      .snapshotChanges().pipe(map(usuarios => usuarios.map(user => {
        /*
         inicializamos un objeto usuario con los datos del usuario obtenido y agregamos otros campos necesarios
         para mostrarlo en la interfaz
        */
        usuario = {
          id: user.payload.doc.id,
          maxFrecuency: 0, // campo indispensable. Es calculado. Contiene la frecuencia maxima para un usuario
          intensities: [], // campo indispensable. Es calculado. Contiene las intensidades, u objetivos propios de un usuario
          _objetivo: { // campo indispensable. Contiene el objetivo del usuario como un objeto
            value: 0,
            text: '',
            objetivo: ''
          },
          tiempo: 0, // campo indispensable. Es calculado. Aloja el tiempo de la sesión del usuario
          baseColor: 0, // campo indispensable. Es calculado. Contiene el color de inicio para este usuario
          intensity: null, // campo indispensable. Es calculado. Contiene el objetivo en el cual se encuentra el usuario
          ... user.payload.doc.data()
        };
        /* calculamos hace cuanto fue la ultima actualizacion del usuario*/
        // diffDates = ((new Date().getTime() - usuario.fecha.toMillis()) / 1000) / 60;
        /*si la ultima actualizacion es mayor a 5 minutos, no se toma en cuenta el registro, se asume que es un
        usuario con sesion terminada*/
        /*if (diffDates > 5) {
          console.log('la diferencia de fechas es invalida o ya han pasado más de 5' +
          ' minutos para la sesion del usuario ' + usuario.nombre, diffDates);
          return null;
        }*/
        /* Calculamos el tiempo inmediatamente sin esperar por la ejecucion del setInterval, que esperara 10 seg
          si esto no se hace, se mostrara en pantalla el usuario, por tener el tiempo en 0, y luego de 10 segundos
          se dejara de mostrar si el tiempo es superior a 30 minutos.*/
        if (usuario.inicio && usuario.fecha) {
          const inicio = usuario.inicio.toMillis();
          // const fin = usuario.fecha.toMillis();
          const fin = new Date().getTime();
          const timeMillis = fin - inicio;
          usuario.tiempo = (timeMillis / 1000) / 60;
        }
        /*console.log('fecha', usuario.fecha.toDate());
        console.log('inicio', usuario.inicio.toDate());
        console.log('new Date()', new Date());*/
        /* Si el usuario no tiene una sesion activa, entonces quiere decir que su reporte
          ya se ha generado. Si tiene un informe generado retornamos null para ignorar este usuario */
        if (usuario.tiempo >= 31) {
          // console.log('El usuario ' + usuario.nombre + ' tiene 30 o más minutos de sesión.');
          return null;
        }

        if (!this.sessions.find(session => session.userId === usuario.id)) {
          console.log('no hay sesion para ' + usuario.id);
          const that = this;
          const userId = usuario.id;
          this.sessions.push({
            userId: usuario.id,
            timer: setInterval(function() {
              // console.log('calculando...');
              const u = userId;
              const tiempo = that.calculateTime(u);
              console.log(new Date() + 'calculando tiempo de ' + userId + ': ' + tiempo + ' minutos');
              if (tiempo === -1) {
                console.log('tiempo invalido. eliminando intervalo de chequeo de tiempo del usuario ', u);
                const index = that.sessions.findIndex(sess => sess.userId === u);
                clearInterval(that.sessions[index].timer);
                /* y sacamos del array de sesiones la sesion del usuario */
                console.log('removiendo sesion de usuario');
                that.sessions.splice(index, 1);
              }
              // comprobamos que el tiempo de sesion del usuario no sea igual o mayor a 30 min
              if (tiempo >= 30) {
                // Si el tiempo de sesion es igual o mayor a 30, generamos el informe para este usuario
                console.log('El usuario ' + u + ' tiene 30 o más minutos de sesión.');
                // buscamos si en el array de informes se encuentra el id del usuario, si no esta, generamos el informe
                if (!that.tieneInforme(u)) {
                  console.log('El usuario ' + u + ' no ha generado su informe aun. Se generará su informe');
                  that.generateInforme(u);
                } else {
                  console.log('El usuario ' + u + ' ya tiene un informe realizado en el dia de hoy.');
                }
                const index = that.sessions.findIndex(sess => sess.userId === u);
                // console.log('index de usuario ' + usuario.nombre + ' en las sesiones es: ' + index);
                if (index >= 0) {
                  console.log('eliminando intervalo de chequeo de tiempo del usuario ', u);
                  clearInterval(that.sessions[index].timer);
                  /* y sacamos del array de sesiones la sesion del usuario */
                  console.log('removiendo sesion de usuario');
                  that.sessions.splice(index, 1);
                }
              }
            }, 10000)
          });
          console.log('sesion creada para ' + usuario.nombre);
          // session.timer(usuario, this);
          console.log('sessiones creadas', this.sessions);
        }
        // calculamos la frecuencia maxima del usuario
        this.getMaxPulseFrecuency(usuario);
        // calculamos el color base del usuario segun su edad, se usa el ultimo digito de la misma
        usuario.baseColor = this.generateBaseColor(usuario.edad);
        // obtenemos las intensidades u objetivos para este usuario
        usuario.intensities = this.initIntensities(usuario);
        // obtenemos en cual objetivo o intensidad se encuentra, segun su ritmo cardiaco actual
        usuario.intensity = this.getIntensity(usuario);
        // obtenemos el objeto objetivo del usuario segun el objetivo (numero) establecido para él
        this.getObjetivo(usuario);
        return usuario;
      })[0])).subscribe(usu => {
        /*
        si el usuario es valido, lo asignamos al campo usuario del objeto aux (que corresponde a la
        banda actual)
        */
        if (usu !== undefined && usu !== null) {
          aux.usuario = usu;
          this.bandasPlain.forEach(b => {
            if (b.usuario.id === usu.id) {
              b.usuario = usu;
            }
          });
          console.log('bandasPlain', this.bandasPlain);
          console.log('usuario', usu);
        } else {
          /*
          sino es valido (en caso que tenga un tiempo de sesion >= 30 o no exista un usuario), asignamos
          un usuario vacio con el tiempo en 30, para que en caso de que ya se este mostrando un usuario
          en pantalla, este deje de mostrarse
          */
          aux.usuario = {
            intensity: {
              value: 0,
              text: '',
              objetivo: '',
              color: ''
            },
            tiempo: 30
          };
        }
      });
      return aux;
    })));
    this.bandas.subscribe(bandas => {
      this.bandasPlain = bandas;
      console.log('bandasPlain', this.bandasPlain);
      this.numFilas = bandas.length / this.numColumnas;
    });
  }
  /*
  Funcion para saber si el usuario ya ha generado un informe en el dia de hoy
  */
  tieneInforme(userId: any): boolean {
    const storageObject: any = localStorage.getItem(userId);
    if (storageObject) {
      if (storageObject === this.fecha.toLocaleString()) {
        console.log(`El usuario ${userId} ya tiene un informe generado en el día de hoy`);
        return true;
      } else {
        console.log(`El usuario ${userId} no tiene un informe generado en el día de hoy
        pero si de la fecha ${storageObject}`);
        localStorage.removeItem(userId);
      }
    } else {
      console.log(`El usuario ${userId} no tiene informes generados anteriormente`);
    }
    return false;
  }
  /*
  Funcion para generar informe de la sesion del usuario
  */
  generateInforme(userId, reintento: number = 1) {
    this.bandasPlain.forEach(banda => {
      if (banda.usuario.id === userId) {
        const usuario = banda.usuario;
        const fechaInicio = usuario.inicio.toMillis();
        // const fechaInicio = new Date('January 20, 2020 08:30:00').getTime();
        const sesionTime = 30 * 60 * 1000;
        // fecha con 30 min adicionales a fechaInicio
        const fechaFin = new Date(fechaInicio + sesionTime);
        const pulsaciones = [];
        const estadisticas = [];
        let endDate: any;
        let beginDate: any;
        let gap = 0;
        // guardamos en local bd el id de usuario junto a la fecha para marcar que ya se ha generado su informe
        localStorage.setItem(usuario.id, this.fecha.toLocaleString());
        // Obtenemos las pulsaciones del usuario desde la fecha de inicio de la sesion hasta 30 minutos despues
        this.db.doc('usuarios/' + usuario.id).collection('pulsaciones',
        ref => ref.where('fecha', '>=', usuario.inicio).where('fecha', '<=', fechaFin)).
        get().subscribe(results => {
          if (!results) {
            console.log('No hay pulsaciones para enviar informe al usuario ' + usuario.nombre);
            this.snack.open('No hay pulsaciones para enviar informe al usuario ' + usuario.nombre, 'OK', {
              duration: 2000,
            });
            return;
          }
          let pulsacion;
          results.forEach(doc => {
            pulsacion = {id: doc.id, ...doc.data()};
            if (pulsacion.ritmo > 0) {
              pulsaciones.push(pulsacion);
            }
          });
          if (!pulsaciones[0]) {
            console.log('No hay pulsaciones para enviar informe al usuario ' + usuario.nombre);
            this.snack.open('No hay pulsaciones para enviar informe al usuario ' + usuario.nombre, 'OK', {
              duration: 2000,
            });
            return;
          }
          /*
          El siguiente proceso calcula el tiempo que se dedicó a cada objetivo
          */
          beginDate = pulsaciones[0].fecha;
          let duracion = 0; // guarda la duracion de tiempo en un objetivo
          // iteramos todos los objetivos o intensidades del usuario
          usuario.intensities.forEach((intensity, index) => {
            duracion = 0;
            // iteramos todas las pulsaciones
            for (let i = 1; i < pulsaciones.length; i++) {
              endDate = pulsaciones[i].fecha;
              // gap es el periodo de tiempo entre la ultima pulsacion y la actual
              gap = (endDate.toMillis() - beginDate.toMillis());
              /*
              si el ritmo de la pulsacion actual es >= al ultimo ritmo y <= al ritmo del objetivo actual
              sumamos el gap de tiempo a la duracion
              */
              if (index < usuario.intensities.length - 1) {
                if (pulsaciones[i].ritmo >= (index === 0 ? 0 : intensity.value) &&
                    (index === usuario.intensities.length - 1 ? true : pulsaciones[i].ritmo <= usuario.intensities[index + 1].value)) {
                  duracion += gap;
                }
              }
              beginDate = endDate;
            }
            // si la duracion es menor a 0, lo hacemos igual a 0
            if (duracion <= 0) {
              console.error(`duracion con valor <= 0: ${duracion}`, intensity);
            }
            duracion = duracion < 0 ? 0 : duracion;
            // al finalizar la iteracion de las pulsaciones, insertamos en un array las estadisticas obtenidas
            estadisticas.push({
              tiempo: Number('' + ((duracion / 1000) / 60).toFixed(2)), // tiempo de duracion en este objetivo, en minutos
              objetivo: intensity.text, // nombre del objetivo
              ritmo: intensity.value // ritmo cardiaco del objetivo
            });
          });
          // Enviar estadisticas para generar grafica
          const data = {
            data: estadisticas,
            user: usuario,
            // colors: this.colors
          };
          this.enviarCorreo(data);
        });
      }
    });
  }
  enviarCorreo(datos: any, reintentos: number = 1) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    };
    // console.log('datos a enviar', datos);
    if (reintentos > 3) {
      return null;
    }
    this.http.post<any>('https://experienciaplay.com/gym-api/api/sendReporte2', datos, httpOptions)
    .pipe(retry(3), catchError(this.handleError)).subscribe(result => {
      if (result !== undefined) {
        // console.log('result', result);
        if (result.resp === 'OK') {
          console.log('Informe enviado satisfactoriamente');
          this.snack.open('Informe enviado satisfactoriamente', 'OK', {
            duration: 2000,
          });
          return;
        } else {
          /*
          si no se logra enviar el informe, se muestra un mensaje y se reintenta hasta 3 veces.
          */
          console.log('Informe no enviado debido a un error', result.message);
          /*this.snack.open('Informe no enviado debido a un error en los datos', 'OK', {
            duration: 2000,
          });*/
          localStorage.removeItem(datos.user.id);
        }
      }
    }, error => {
      /*
      si no se logra enviar el informe, se muestra un mensaje y se reintenta hasta 3 veces.
      */
      console.error('Error al enviar informe por correo');
      /* this.snack.open('Error al enviar informe por correo', 'OK', {
          duration: 2000,
      });*/
      // this.enviarCorreo(datos, reintentos++);
      localStorage.removeItem(datos.user.id);
    });
  }
  /*
  Funcion para calcular el tiempo de la sesion del usuario en minutos
  */
  calculateTime(userId: any): number {
    // console.log('buscando en bandasPlain', this.bandasPlain);
    let tiempo = 0;
    this.bandasPlain.forEach(banda => {
      if (banda.usuario.id === userId && banda.usuario.inicio) {
        const inicio = banda.usuario.inicio.toMillis();
        // const fin = usuario.fecha.toMillis();
        const fin = new Date().getTime();
        const timeMillis = fin - inicio;
        tiempo = (timeMillis / 1000) / 60;
      }
    });
    return tiempo;
  }
  /*
  Funcion para obtener la imagen a mostrar como indicador (flecha arriba, flecha abajo o check)
  */
  getIndicador(usuario: any): any {
    const indicador = {
      icon: 'assets/images/arriba.png'
    }; // icono a mostrar, flecha arriba o flecha abajo
    const intensity = usuario.intensity;
    const objetivo = Number(usuario.objetivo);
    const ritmo = Number(usuario.ritmo);
    if (intensity !== undefined && intensity !== null) {
      if (ritmo < usuario._objetivo.value) {
        indicador.icon = 'assets/images/arriba.png';
      } else if (ritmo >= usuario._objetivo.value &&
          ((objetivo < usuario.intensities.length) &&
          (ritmo < usuario.intensities[objetivo].value)) ||
          (usuario.intensities.length === objetivo)) {
            indicador.icon = 'assets/images/check.png';
      } else {
        indicador.icon = 'assets/images/abajo.png';
      }
    }
    return indicador;
  }
  /*
  Funcion para obtener el objetivo en el cual se encuentra el usuario, segun su ritmo cardiaco
  */
  getIntensity(usuario: any): any {
    const ritmo = Number(usuario.ritmo);
    let last = null;
    if (usuario.intensities !== undefined && usuario.intensities != null) {
      usuario.intensities.forEach(intensity => {
        if (ritmo >= intensity.value || last == null) {
          last = intensity;
        }
      });
    }
    return new Object(last);
  }
  /*
  Funcion para obtener los datos del objetivo establecido para el usuario
  */
  getObjetivo(usuario: any) {
    const objetivo = Number(usuario.objetivo);
    if (usuario.intensities !== undefined && usuario.intensities != null && usuario.intensities[objetivo - 1] !== undefined) {
      usuario._objetivo = usuario.intensities[objetivo - 1];
      usuario._objetivo.value = Math.floor(usuario._objetivo.value);
    }
  }
  /*
  Funcion para obtener la frecuencia maxima del usuario
  */
  getMaxPulseFrecuency(user: any) {
    let frecuency = 0;
    user.sexo = 'M'; // TODO: quitar esto
    if (user.sexo === 'M') {
      frecuency = 213 - (0.789 * Number(user.edad));
    }
    if (user.sexo === 'F') {
      frecuency = 208 - (0.789 * Number(user.edad));
    }
    user.maxFrecuency = Math.floor(frecuency);
  }
  /*
  Funcion para crear el array de objetivos o intensidades para un usuario
  */
  initIntensities(usuario: any) {
    const values = [];
    const gaps = [59, 69, 79, 89, 100];
    let intensity = null;
    gaps.forEach(value => {
      const calc = Math.floor((value * usuario.maxFrecuency) / 100);
      intensity = JSON.parse(JSON.stringify({
        value: 0,
        text: '',
        objetivo: '',
        color: ''
      }));

      if (value >= 0 && value <= 59) {
        intensity.color = usuario.baseColor;
        intensity.text = 'Reduciendo Stress';
        intensity.objetivo = 'Reducir Stress';
      }
      if (value >= 60 && value <= 69) {
        intensity.color = this.getNextColor(usuario.baseColor, 1);
        intensity.text = 'Moviéndose Más';
        intensity.objetivo = 'Mover Más';
      }
      if (value >= 70 && value <= 79) {
        intensity.color = this.getNextColor(usuario.baseColor, 2);
        intensity.text = 'Perdiendo Peso';
        intensity.objetivo = 'Perder Peso';
      }
      if (value >= 80 && value <= 89) {
        intensity.color = this.getNextColor(usuario.baseColor, 3);
        intensity.text = 'Tonificando';
        intensity.objetivo = 'Tonificar';
      }
      if (value >= 90 && value <= 100) {
        intensity.color = this.getNextColor(usuario.baseColor, 4);
        intensity.text = 'Mejorando Rendimiento';
        intensity.objetivo = 'Mejorar Rendimiento';
      }
      intensity.value = calc;
      values.push(intensity);
    });

    return values;
  }
  /*
  Funcion para generar el color base de un usuario
  Se obtiene un numero entre 0 y 4
  */
  generateBaseColor(edad) {
    const segundoDigito = edad % 10;
    return Math.floor(segundoDigito / 2);
  }
  /*
  Funcion para obtener el color hexadecimal segun una posicion
  */
  getHexColor(colorPos) {
    return this.colors[colorPos];
  }
  /*
  Funcion para obtener el siguiente color en el array de colores, segun la posicion de otro color
  */
  getNextColor(colorPos, positions) {
    let currPos = colorPos;
    for (let i = 0; i < positions; i++) {
      if (currPos === this.colors.length - 1) {
        currPos = 0;
      } else {
        currPos++;
      }
    }
    return currPos;
  }
  private handleError(error: HttpErrorResponse) {
    let mensaje = 'Error al hacer la peticion al servidor';
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      mensaje = error.error.message;
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }
}

interface Banda {
  mac: string;
  numero: string;
  usuario: any;
  id: string;
}
interface Usuario {
  nombre: string;
  objetivo: string;
  _objetivo: any;
  edad: string;
  banda: string;
  email: string;
  ritmo: string;
  baseColor: string;
  pulsaciones: Observable<any[]>;
}
