import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Validators, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Observable, throwError, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import { Session } from '../../models/session.model';
import { Renderer2, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})

export class TestComponent implements AfterViewInit {
  alert: any = {
    message: '',
    type: 'error',
    show: false
  };
  loadingResults = false;
  loadingUsers = false;
  testForm: FormGroup;
  usuarios = [];
  tiempoSesion = 30;
  reporte: any = null;
  colors = ['amarillo', 'azul', 'rojo', 'violeta', 'verde']; // array clases utilizadas para el panel
  colors2 = ['#e8d500', '#009cff', '#ff1500', '#ea3df7', '#8eff79']; // array colores utilizados para el test
  constructor(private db: AngularFirestore, private snack: MatSnackBar, private renderer2: Renderer2,
              @Inject(DOCUMENT) private _document, private formBuilder: FormBuilder) {

    this.testForm = this.formBuilder.group({
      tiempoSesion: [this.tiempoSesion],
      usuario: [null, Validators.required],
      fechaInicio: [''],
      horaInicio: ['12:00 AM']
    });
    // obtenemos la lista de usuarios
    this.loadingUsers = true;
    this.db.collection('usuarios').get().subscribe(results => {

      this.loadingUsers = false;
      results.forEach(user => this.usuarios.push({id: user.id, ...user.data()}));
      console.log('usuarios', this.usuarios);
    });
  }
  ngAfterViewInit(): void {
  }
  /*
  Funcion para generar informe de la sesion del usuario
  */
  generateInforme() {
    const userId = this.testForm.get('usuario').value;
    console.log('usuario seleccionado', userId);
    const usuario = this.usuarios.find(user => user.id === userId);
    if (usuario.id === userId) {
      this.loadingResults = true;
      this.alert.show = false;
      // configuramos parametros esenciales
      // calculamos la frecuencia maxima del usuario
      this.getMaxPulseFrecuency(usuario);
      // calculamos el color base del usuario segun su edad, se usa el ultimo digito de la misma
      usuario.baseColor = this.generateBaseColor(usuario.edad);
      // obtenemos las intensidades u objetivos para este usuario
      usuario.intensities = this.initIntensities(usuario);
      // obtenemos el objeto objetivo del usuario segun el objetivo (numero) establecido para él
      this.getObjetivo(usuario);
      // fin de configuracion de parametros esenciales
      console.log('datos del usuario antes del calculo de tiempo', usuario);
      let fechaInicio;
      console.log('input fecha inicio', this.testForm.get('fechaInicio').value);
      if (this.testForm.get('fechaInicio').value.length > 0) {
        let hora = '12:00 AM';
        if (this.testForm.get('horaInicio').value.length > 0) {
          hora = this.testForm.get('horaInicio').value;
        } else {
          console.log('hora de inicio por defecto', hora);
          this.testForm.get('horaInicio').setValue(hora);
        }
        fechaInicio = new Date(`${this.testForm.get('fechaInicio').value} ${hora}`);
        console.log('fecha de inicio seleccionada', fechaInicio);
        fechaInicio = fechaInicio.getTime();
      } else if (usuario.inicio) {
        fechaInicio = usuario.inicio.toMillis();
        this.testForm.get('fechaInicio').setValue(new Intl.DateTimeFormat('en-US').format(usuario.inicio.toMillis()));
        console.log('fecha de inicio de ultima sesion del usuario', new Date(fechaInicio));
      } else {
        console.error('El usuario no tiene una fecha de inicio y no se ha indicado una fecha de inicio');
        this.alert.message = 'El usuario no tiene una fecha de inicio y no se ha indicado una fecha de inicio';
        this.alert.type = 'error';
        this.alert.show = true;
        this.loadingResults = false;
        return;
      }
      let time = this.tiempoSesion;
      let sesionTime;
      // const fechaInicio = new Date('January 20, 2020 08:30:00').getTime();
      if (this.testForm.get('tiempoSesion').value > 0) {
        time = Number(this.testForm.get('tiempoSesion').value);
        console.log('input tiempo', time);
      } else {
        console.log('tiempo por defecto', time);
        this.testForm.get('tiempoSesion').setValue(time);
      }
      sesionTime = time * 60 * 1000;
      // fecha con 30 min adicionales a fechaInicio
      const fechaFin = new Date(fechaInicio + sesionTime);
      console.log('fecha fin calculada', fechaFin);
      const pulsaciones: any[] = [];
      const estadisticas = [];
      let endDate: any;
      let beginDate: any;
      let gap = 0;

      // Obtenemos las pulsaciones del usuario desde la fecha de inicio de la sesion hasta 30 minutos despues
      this.db.doc('usuarios/' + usuario.id).collection('pulsaciones',
      ref => ref.where('fecha', '>=', new Date(fechaInicio)).where('fecha', '<=', fechaFin)).
      get().subscribe(results => {
        if (!results) {
          console.log('No hay pulsaciones para enviar informe al usuario ' + usuario.nombre);
          this.snack.open('No hay pulsaciones para enviar informe al usuario ' + usuario.nombre, 'OK', {
            duration: 2000,
          });
          this.alert.message = 'No hay pulsaciones dentro del rango de tiempo seleccionado';
          this.alert.type = 'error';
          this.alert.show = true;
          this.loadingResults = false;
          return;
        }
        let pulsacion;
        results.forEach(doc => {
          pulsacion = {id: doc.id, ...doc.data()};
          if (pulsacion.ritmo > 0) {
            pulsaciones.push(pulsacion);
          }
        });
        if (!pulsaciones[0]) {
          console.log('No hay pulsaciones para enviar informe al usuario ' + usuario.nombre);
          this.snack.open('No hay pulsaciones para enviar informe al usuario ' + usuario.nombre, 'OK', {
            duration: 2000,
          });
          this.loadingResults = false;
          this.alert.message = 'No hay pulsaciones dentro del rango de tiempo seleccionado';
          this.alert.type = 'error';
          this.alert.show = true;
          return;
        }
        console.log('pulsaciones encontradas dentro del intervalo', pulsaciones);
        /*
        El siguiente proceso calcula el tiempo que se dedicó a cada objetivo
        */
        beginDate = pulsaciones[0].fecha;

        // lastPulse = pulsaciones[0].ritmo;
        let duracion = 0; // guarda la duracion de tiempo en un objetivo
        // iteramos todos los objetivos o intensidades del usuario
        usuario.intensities.forEach((intensity, index) => {
          duracion = 0;
          // iteramos todas las pulsaciones
          for (let i = 1; i < pulsaciones.length; i++) {
            endDate = pulsaciones[i].fecha;
            // gap es el periodo de tiempo entre la ultima pulsacion y la actual
            gap = (endDate.toMillis() - beginDate.toMillis());
            /*
            si el ritmo de la pulsacion actual es >= al ultimo ritmo y <= al ritmo del objetivo actual
            sumamos el gap de tiempo a la duracion
            */
            if (index < usuario.intensities.length - 1) {
              if (pulsaciones[i].ritmo >= (index === 0 ? 0 : intensity.value) &&
                  (index === usuario.intensities.length - 1 ? true : pulsaciones[i].ritmo <= usuario.intensities[index + 1].value)) {
                duracion += gap;
              }
            }
            beginDate = endDate;
          }
          // si la duracion es menor a 0, lo hacemos igual a 0
          if (duracion <= 0) {
            console.error(`duracion con valor <= 0: ${duracion}`, intensity);
          }
          duracion = duracion < 0 ? 0 : duracion;
          // al finalizar la iteracion de las pulsaciones, insertamos en un array las estadisticas obtenidas
          estadisticas.push({
            tiempo: Number('' + ((duracion / 1000) / 60).toFixed(2)), // tiempo de duracion en este objetivo, en minutos
            objetivo: intensity.text, // nombre del objetivo
            ritmo: intensity.value, // ritmo cardiaco del objetivo
            color: this.colors2[intensity.color]
          });
        });
        // Enviar estadisticas para generar grafica
        const data = {
          data: estadisticas,
          user: usuario,
          // colors: this.colors
        };
        this.alert.message = 'Datos recuperados con exito';
        this.alert.type = 'success';
        this.alert.show = true;
        console.log('data for report', data);
        this.reporte = data;
        this.loadingResults = false;
      });
    }
  }

  /*
  Funcion para obtener los datos del objetivo establecido para el usuario
  */
  getObjetivo(usuario: any) {
    const objetivo = Number(usuario.objetivo);
    if (usuario.intensities !== undefined && usuario.intensities != null && usuario.intensities[objetivo - 1] !== undefined) {
      usuario._objetivo = usuario.intensities[objetivo - 1];
      usuario._objetivo.value = Math.floor(usuario._objetivo.value);
    }
  }
  /*
  Funcion para obtener la frecuencia maxima del usuario
  */
  getMaxPulseFrecuency(user: any) {
    let frecuency = 0;
    user.sexo = 'M'; // TODO: quitar esto
    if (user.sexo === 'M') {
      frecuency = 213 - (0.789 * Number(user.edad));
    }
    if (user.sexo === 'F') {
      frecuency = 208 - (0.789 * Number(user.edad));
    }
    user.maxFrecuency = Math.floor(frecuency);
  }
  /*
  Funcion para crear el array de objetivos o intensidades para un usuario
  */
  initIntensities(usuario: any) {
    const values = [];
    const gaps = [59, 69, 79, 89, 100];
    let intensity = null;
    gaps.forEach(value => {
      const calc = Math.floor((value * usuario.maxFrecuency) / 100);
      intensity = JSON.parse(JSON.stringify({
        value: 0,
        text: '',
        objetivo: '',
        color: ''
      }));

      if (value >= 0 && value <= 59) {
        intensity.color = usuario.baseColor;
        intensity.text = 'Reduciendo Stress';
        intensity.objetivo = 'Reducir Stress';
      }
      if (value >= 60 && value <= 69) {
        intensity.color = this.getNextColor(usuario.baseColor, 1);
        intensity.text = 'Moviéndose Más';
        intensity.objetivo = 'Mover Más';
      }
      if (value >= 70 && value <= 79) {
        intensity.color = this.getNextColor(usuario.baseColor, 2);
        intensity.text = 'Perdiendo Peso';
        intensity.objetivo = 'Perder Peso';
      }
      if (value >= 80 && value <= 89) {
        intensity.color = this.getNextColor(usuario.baseColor, 3);
        intensity.text = 'Tonificando';
        intensity.objetivo = 'Tonificar';
      }
      if (value >= 90 && value <= 100) {
        intensity.color = this.getNextColor(usuario.baseColor, 4);
        intensity.text = 'Mejorando Rendimiento';
        intensity.objetivo = 'Mejorar Rendimiento';
      }
      intensity.value = calc;
      values.push(intensity);
    });

    return values;
  }
  /*
  Funcion para generar el color base de un usuario
  Se obtiene un numero entre 0 y 4
  */
  generateBaseColor(edad) {
    const segundoDigito = edad % 10;
    return Math.floor(segundoDigito / 2);
  }
  /*
  Funcion para obtener el color hexadecimal segun una posicion
  */
  getHexColor(colorPos) {
    return this.colors[colorPos];
  }
  /*
  Funcion para obtener el siguiente color en el array de colores, segun la posicion de otro color
  */
  getNextColor(colorPos, positions) {
    let currPos = colorPos;
    for (let i = 0; i < positions; i++) {
      if (currPos === this.colors.length - 1) {
        currPos = 0;
      } else {
        currPos++;
      }
    }
    return currPos;
  }

}

interface Usuario {
  id?: any;
  nombre?: string;
  objetivo?: any;
  _objetivo?: any;
  edad?: any;
  banda?: any;
  email?: string;
  fecha?: any;
  inicio?: any;
  ritmo?: any;
  baseColor?: any;
  pulsaciones?: Observable<any[]>;
  intensities?: any[];
}
